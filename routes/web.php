<?php

Route::name('movies.index')
    ->get('/', 'MovieController@index');

Route::name('movies.show')
    ->get('/movies/{movie}', 'MovieController@show');

Route::name('actors.show')
    ->get('/actors/{actor}', 'ActorController@show');

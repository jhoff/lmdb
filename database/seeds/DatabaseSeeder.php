<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Actor::class, 400)
            ->create();

        factory(App\Movie::class, 40)
            ->create()
            ->each(function ($movie) {
                $movie
                    ->actors()
                    ->attach(
                        App\Actor::inRandomOrder()
                            ->limit(rand(20, 40))
                            ->get()
                    );
            });
    }
}

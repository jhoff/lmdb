<?php

/*
|--------------------------------------------------------------------------
| Actor Factory
|--------------------------------------------------------------------------
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Actor::class, function (Faker\Generator $faker) {
    return [
        'gender' => $gender = $faker->randomElement(['male','female']),
        'name' => ( $gender === 'male' ? $faker->firstNameMale : $faker->firstNameFemale ) . ' ' . $faker->lastName,
        'bio' => $faker->paragraph,
        'birth_date' => $faker->dateTimeThisCentury,
    ];
});

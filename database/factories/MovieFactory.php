<?php

/*
|--------------------------------------------------------------------------
| Actor Factory
|--------------------------------------------------------------------------
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Movie::class, function (Faker\Generator $faker) {
    return [
        'title' => ucfirst($faker->words(5, true)),
        'synopsis' => $faker->paragraph,
        'release_date' => $faker->dateTimeThisDecade,
        'rating' => $faker->randomElement(['G', 'PG', 'PG-13', 'R', 'NC-17']),
        'length' => $faker->numberBetween(30, 180),
        'stars' => $faker->numberBetween(1,5),
    ];
});

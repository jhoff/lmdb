<?php

namespace App\Http\Controllers;

use App\Actor;

class ActorController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function show(Actor $actor)
    {
        return view('actors.show')
            ->withActor($actor);
    }
}

# LinkedIn Movie Database

```
# Install dependencies
composer install

# Create the database
touch database/database.sqlite

# Migrate and seed database
php artisan migrate --seed
```

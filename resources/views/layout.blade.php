<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LinkedIn Movie Database</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.1/css/bulma.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="app">
            @include('navigation')

            <section class="hero is-info">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            @yield('hero')
                        </h1>
                    </div>
                </div>
            </section>

            <section class="section">
                <div class="container">
                    @yield('content')
                </div>
            </section>
        </div>
    </body>
</html>

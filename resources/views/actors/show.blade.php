@extends('layout')

@section('hero')
    <span class="icon is-large">
        <i class="fa fa-user"></i>
    </span>
    {{ $actor->name }}
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <figure class="image">
                <img style="width:240px; height:240px;" src="https://randomuser.me/api/portraits/{{ $actor->gender === 'male' ? 'men' : 'women' }}/{{ $actor->id % 99 }}.jpg">
            </figure>
        </div>
        <div class="media-content">
            <div class="content">
                <p>{{ $actor->bio }}</p>
                <p><b>Born:</b> {{ $actor->birth_date->toDateString() }}</p>
            </div>
        </div>
    </div>
@endsection

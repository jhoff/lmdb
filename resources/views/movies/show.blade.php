@extends('layout')

@section('hero')
    <span class="icon is-large">
        <i class="fa fa-video-camera"></i>
    </span>
    {{ $movie->title }}
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <figure class="image">
                <img src="https://placeimg.com/280/418/any?{{ $movie->id }}">
            </figure>
        </div>
        <div class="media-content">
            <div class="content">
                <p>{{ $movie->synopsis }}</p>
                <p>
                    @for ($i = 0; $i < $movie->stars; $i++)
                        <span class="icon has-text-warning">
                            <i class="fa fa-star"></i>
                        </span>
                    @endfor
                </p>
                <p>
                    <span class="tag is-dark">{{ $movie->rating }}</span> |
                    {{ $movie->release_date->toDateString() }} |
                    {{ $movie->length }} min
                </p>
            </div>
        </div>
    </div>

    <hr />

    <p class="title is-2">Cast</p>

    <table class="table is-striped is-narrow is-fullwidth">
        @foreach($movie->actors as $actor)
            <tr>
                <td width="32">
                    <a href="{{ route('actors.show', $actor) }}">
                        <figure class="image is-32x32">
                            <img src="https://randomuser.me/api/portraits/{{ $actor->gender === 'male' ? 'men' : 'women' }}/{{ $actor->id % 99 }}.jpg">
                        </figure>
                    </a>
                </td>
                <td>
                    <a href="{{ route('actors.show', $actor) }}">
                        <span>{{ $actor->name }}</span>
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection

@extends('layout')

@section('hero')
    <span class="icon is-large">
        <i class="fa fa-film"></i>
    </span>
    All Movies
@endsection

@section('content')
    @foreach($movies->chunk(2) as $row)
        <div class="columns">
            @foreach($row as $movie)
                <div class="column is-one-half">
                    <div class="media">
                        <div class="media-left">
                            <a href="{{ route('movies.show', $movie) }}">
                                <figure class="image">
                                    <img style="height:209px; width:140px;" src="https://placeimg.com/280/418/any?{{ $movie->id }}">
                                </figure>
                            </a>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <a href="{{ route('movies.show', $movie) }}">
                                    <p class="title is-4">{{ $movie->title }}</p>
                                </a>
                                <p>{{ $movie->synopsis }}</p>
                                <p>
                                    @for ($i = 0; $i < $movie->stars; $i++)
                                        <span class="icon has-text-warning">
                                            <i class="fa fa-star"></i>
                                        </span>
                                    @endfor
                                </p>
                                <p>
                                    <span class="tag is-dark">{{ $movie->rating }}</span> |
                                    {{ $movie->release_date->toDateString() }} |
                                    {{ $movie->length }} min
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    {{ $movies->links() }}
@endsection

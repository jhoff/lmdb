<nav class="navbar ">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            <span class="icon is-large">
                <i class="fa fa-imdb"></i>
            </span>
            LinkedIn Movie Database
        </a>
    </div>
</div>
</nav>
